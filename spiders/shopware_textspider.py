# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from urllib.parse import urljoin
import csv

class ShopwareTextspiderSpider(CrawlSpider):
    name = 'shopware_textspider'
    allowed_domains = ['mma-industries.de']
    start_urls = ['http://mma-industries.de/']

    def __init__(self):
        self.csvwriter = csv.writer(open('text_link_mma-industries.de.txt', 'w'))
        super().__init__(self)
        self.output = open('text_mma-industries.de.html','w')

    rules = (
        Rule(LinkExtractor(allow=r'/'), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        i = {}
        #i['domain_id'] = response.xpath('//input[@id="sid"]/@value').extract()
        #i['name'] = response.xpath('//div[@id="name"]').extract()
        #i['description'] = response.xpath('//div[@id="description"]').extract()
        #text = response.xpath('//head/meta[@name='title']').extract()
        text = response.xpath('//div[@class="html--content panel--body is--wide"]/p').extract()
        #text2 = response.xpath('//head/meta[@name="keywords"]').extract()
        self.csvwriter.writerow([response.url, text])
        self.output.write("<h2>%s</h2>\n"%(response.url))
        self.output.write("<ul>\n")
        for te in text:
            outStr = '<li>{}</li>\n'.format(te)
            self.output.write(outStr)

        self.output.write("</ul>\n")
        return i
