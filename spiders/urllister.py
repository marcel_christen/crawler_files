# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

import csv

class UrllisterSpider(CrawlSpider):
    name = 'urllister'
    allowed_domains = ['karateanzug.ninja, amazon.de']
    start_urls = ['https://karateanzug.ninja']

    def __init__(self):
        self.urllist = []
        super().__init__(self)


    rules = (
        Rule(LinkExtractor(allow=r'/'), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        i = {}
        #i['domain_id'] = response.xpath('//input[@id="sid"]/@value').extract()
        #i['name'] = response.xpath('//div[@id="name"]').extract()
        #i['description'] = response.xpath('//div[@id="description"]').extract()
        print(response.url)
        self.urllist.append(response.url)
        return i

    def closed(self, reason):
        print("Fertig")
        print("Anzahl URLs: ", len(self.urllist))
        csvwriter = csv.writer(open('links_karate_intern.csv', 'w'))
        for url in self.urllist:
           csvwriter.writerow([url])
