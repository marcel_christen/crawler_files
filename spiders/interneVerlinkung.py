# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from urllib.parse import urljoin
import csv

class InterneverlinkungSpider(CrawlSpider):
    name = 'interneVerlinkung'
    allowed_domains = ['snowtimes.de']
    # = ['internet-dienste-mueller.de']
    #allowed_domains = ['bayern-gold.de']
    #allowed_domains = ['vr-expert.de']
    start_urls = ['https://www.snowtimes.de/']

    def __init__(self):
        self.csvwriter = csv.writer(open('Verlinkung_snowtimes.csv', 'w'))
        super().__init__(self)
        self.output = open('Verlinkung_snowtimes.html','w')


    rules = (
        Rule(LinkExtractor(allow=r'/'), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        i = {}
        text = response.xpath('//a/@href').extract()
        #self.csvwriter.writerow([response.url, text])
        #self.output.write("<h2>%s</h2>\n"%(response.url))
        #self.output.write("<ul>\n")
        self.csvwriter.writerow(["Source,Target"])
        #text = [te.strip(' ') for te in text]
        for te in text:
            #if te == str("https://karateanzug.ninja#content"):
                #te ="https://karateanzug.ninja"
            outStr = '{}'.format(urljoin('https://www.snowtimes.de/', te))
            #self.output.write(outStr)
            self.csvwriter.writerow([response.url, outStr])
        #print(te)

        #self.output.write("</ul>\n")
        return i
