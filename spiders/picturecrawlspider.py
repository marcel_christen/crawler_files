# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from urllib.parse import urljoin
import csv



class PicturecrawlspiderSpider(CrawlSpider):
    name = 'picturecrawlspider'
    allowed_domains = ['karateanzug']
    start_urls = ['https://karateanzug.ninja']

    def __init__(self):
        self.csvwriter = csv.writer(open('links_karateanzug.ninja.txt', 'w'))
        super().__init__(self)
        self.output = open('images_karateanzug.ninja.html','w')

    rules = (
        Rule(LinkExtractor(allow=r'/'), callback='parse_item', follow=True),
    )

    def parse_item(self, response):
        i = {}
        #i['domain_id'] = response.xpath('//input[@id="sid"]/@value').extract()
        #i['name'] = response.xpath('//div[@id="name"]').extract()
        #i['description'] = response.xpath('//div[@id="description"]').extract()
        imgs = response.xpath('//img/@src').extract()
        self.csvwriter.writerow([response.url, imgs])
        self.output.write("<h2>%s</h2>\n"%(response.url))
        self.output.write("<ul>\n")
        for im in imgs:
            outStr = '<li><img src="{}"></li>\n'.format(urljoin('https://karateanzug.ninja', im))
            self.output.write(outStr)

        self.output.write("</ul>\n")

        return i
